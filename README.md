# ampe_stor
Driver for Alcor Micro PCI-Express card reader
plus concerning gentoo ebuild to build the kernel module...

 * works at least - not very stable - with my gentoo-sources-4.12.6 kernel
 * up to now several hangups / ro-remounts during copy actions from my 64GB sdcard (vfat) to harddisk 
(don't try to repair the sdcard file system in case the systems tells you that the file system of sdcard is corrupt... then it's really corrupt)
 * 20180521 made it compile again with newer kernels (4.16.10), but still the same buggy behaviour ! If the sdcard contains roughly > ~1G it still thinks that the file system is corrupt (there seems to some integer overruns in accessing the device or something similar)

```
[ 2511.460487] FAT-fs (sdb1): error, invalid access to FAT (entry 0xa4606262)
[ 2511.460490] FAT-fs (sdb1): Filesystem has been set read-only
[ 2511.460624] FAT-fs (sdb1): error, invalid access to FAT (entry 0xaf2e4c83)
[ 2511.460752] FAT-fs (sdb1): error, invalid access to FAT (entry 0xa43b5e07)
[ 2511.460882] FAT-fs (sdb1): error, invalid access to FAT (entry 0x4fc49a36)
[ 2522.886915] FAT-fs (sdb1): error, invalid access to FAT (entry 0xb2028bbc)
[ 2522.887022] FAT-fs (sdb1): error, invalid access to FAT (entry 0x2769294f)
[ 2522.887045] FAT-fs (sdb1): error, invalid access to FAT (entry 0x36b6881f)
[ 2522.887068] FAT-fs (sdb1): error, invalid access to FAT (entry 0x3bc43a2e)
[ 2522.887131] FAT-fs (sdb1): error, invalid access to FAT (entry 0xbff8a489)
[ 2522.887202] FAT-fs (sdb1): error, invalid access to FAT (entry 0xecf4a73c)
[ 2522.887243] FAT-fs (sdb1): error, invalid access to FAT (entry 0x29233cea)
[ 2522.887345] FAT-fs (sdb1): error, invalid access to FAT (entry 0x9cca0e95)
```



Copyright(c) 2012 Alcor Micro Corp. All rights reserved.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2, or (at your option) any later version.

 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, see <http://www.gnu.org/licenses/>.
